public class MainChallenge {
    public static void main(String[] args) {

        int highScore = calculateScore(true, 800, 5, 100);
        System.out.println("The highScore is " + highScore);

        System.out.println("The highScore is " +
                calculateScore(true, 10_000, 8, 200)
        );

        displayHighScorePosition(
                "Adam",
                calculateHighScorePosition(1500)
        );
        displayHighScorePosition(
                "Bruce",
                calculateHighScorePosition(1000)
        );
        displayHighScorePosition(
                "Charlie",
                calculateHighScorePosition(500)
        );
        displayHighScorePosition(
                "Diane",
                calculateHighScorePosition(100)
        );
        displayHighScorePosition(
                "Elane",
                calculateHighScorePosition(25)
        );
    }

    public static int calculateScore(boolean gameOver, int score, int levelCompleted, int bonus) {

        int finalScore = score;

        if(gameOver == true){
            finalScore += (levelCompleted * bonus);
            finalScore += 1_000;
            System.out.println("Your final score was " + finalScore);
        }

        return finalScore;
    }

    public static void displayHighScorePosition(String playerName, int position){
        System.out.println(playerName + " managed to get into position " + position + " on the high score list");
    }

    public static int calculateHighScorePosition(int score){
        int l_var = 4;
        if (score >= 1000){
            l_var = 1;
        } else if (score >= 500/* && score < 1000*/) {
            l_var = 2;
        } else if (score >= 100/* && score < 500*/) {
            l_var = 3;
        }/*else{
            l_var = 4;
        }*/
        return l_var;
    }
}
