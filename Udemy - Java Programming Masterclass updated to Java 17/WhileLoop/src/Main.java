public class Main {
    public static void main(String[] args) {

        for(int i = 1; i <= 5; i++){
            System.out.println(i);
        }

        int j = 1;
        while (true){
            if(j>5){
                break;
            }
            System.out.println(j);
            j++;
        }

        boolean isReady = false;
        do{
            System.out.println("Do it at least once");
        }while(isReady);
    }
}
