public class LargestPrime {
    public static int getLargestPrime(int number){
        if(number <0){
            return -1;
        }
        int Prime = -1;
        int largestPrime = -1;

        //every prime candidate
        for(int i= 2; i <= number; i++){
            //validate if i is a prime
            for(int j = 2; j <= i; j++){

                if(i%j==0 && i!=j){
                    break;
                }else if(i%j==0 && i==j){
                    Prime = i;
                }

            }
            if(number%Prime==0 && Prime > largestPrime){
                largestPrime = Prime;
            }
        }
        return largestPrime;
    }
}
