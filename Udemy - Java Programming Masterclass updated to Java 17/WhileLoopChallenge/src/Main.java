public class Main {
    public static void main(String[] args) {

        int number = 5;
        int finishNumber = 20;
        int totalEven = 0 ;
        int totalOdd = 0;

        while(number <= finishNumber){
            if(isEvenNumber(number)){
                System.out.println("Even number: " + number);
                totalEven++;
            }else{
                totalOdd++;
            }
            if(totalEven == 5){
                break;
            }
            number++;
        }
        System.out.println("Total even: " + totalEven);
        System.out.println("Total odd: " + totalOdd);
    }

    public static boolean isEvenNumber(int number) {
        if(number % 2 == 0){
            return true;
        }else{
            return false;
        }
    }


}
