public class NumberPalindrome {
    public static boolean isPalindrome(int number){
        int reverse = 0;
        int lastDigit;
        int original = number;

        while(original != 0){
            //extract the last digit
            lastDigit = original%10;

            //increase reverse variable
            reverse*=10;

            //add the lastDigit to the reverse variable, since the last digit of reverse is going to be 0
            reverse = reverse + lastDigit;

            //remove the last digit of original
            original/=10;
        }

        return number == reverse;
    }

}