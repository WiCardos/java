public class Main {
    public static void main(String[] args) {

        System.out.println("From height in 10 inches to " + convertToCentimeters(10) +" centimeters.");
        System.out.println("From height in 5 feet and 10 inches to " + convertToCentimeters(5,10) +" centimeters.");
    }
    public static double convertToCentimeters(int height){
        return height*2.54;
    }

    public static double convertToCentimeters(int heightFeet, int heightInches){
        return convertToCentimeters(heightFeet*12 + heightInches);
    }
}
