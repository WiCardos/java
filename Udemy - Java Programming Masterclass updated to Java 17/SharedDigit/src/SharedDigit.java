public class SharedDigit {
    public static boolean hasSharedDigit(int number1, int number2){
        if(number1 < 10 || number1 > 99 || number2 < 10 || number2 > 99 ){
            return false;
        }
        int valNumber2;
        while(number1>0){
            valNumber2 = number2;
            while(valNumber2>0){
                if(number1%10 == valNumber2%10){
                    return true;
                }
                valNumber2 /= 10;
            }
            number1 /= 10;
        }
        return false;
    }
}
