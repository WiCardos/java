public class Main {
    public static void main(String[] args) {
        System.out.println(SharedDigit.hasSharedDigit(12,23));
        //should return true

        System.out.println(SharedDigit.hasSharedDigit(9,99));
        //should return false

        System.out.println(SharedDigit.hasSharedDigit(15,55));
        //should return true
    }
}
