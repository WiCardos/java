public class PerfectNumber {
    public static boolean isPerfectNumber(int number){

        int perfectCandidate = 0;
        for(int i=1; i<number; i++){
            if(number%i==0){
                perfectCandidate += i;
            }
        }
        return perfectCandidate == number && number > 1;
    }
}
