public class Main {
    public static void main(String[] args) {
        System.out.println(PerfectNumber.isPerfectNumber(6));
        //should return true;

        System.out.println(PerfectNumber.isPerfectNumber(28));
        //should return true;

        System.out.println(PerfectNumber.isPerfectNumber(5));
        //should return false;

        System.out.println(PerfectNumber.isPerfectNumber(-1));
        //should return false;
    }
}
