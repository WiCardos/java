public class Main {
    public static void main(String[] args) {
        int sumNumbers = 0;
        int countNumbers = 0;
        for(int i = 1; i <= 1000; i++){
            if(i%3==0 && i%5==0){
                sumNumbers += i;
                System.out.println("Found a match = " + i);
                countNumbers++;
            }
            if(countNumbers==5){
                break;
            }
        }
        System.out.println("Sum = " + sumNumbers);
    }
}
