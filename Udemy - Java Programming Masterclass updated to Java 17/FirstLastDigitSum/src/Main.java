public class Main {
    public static void main(String[] args) {
        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(-1));
        // should return -1

        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(0));
        // should return 0

        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(1));
        // should return 2

        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(12));
        // should return 3

        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(123));
        // should return 4
    }
}
