public class FirstLastDigitSum {
    public static int sumFirstAndLastDigit(int number){
        if(number <0){
            return -1;
        }else{
            int sum = number%10;        //initializes with the right most digit, but does not remove it
            int leftDigit;

            do{
                leftDigit = number%10;  //adds the right most digit
                number /=10;            //removes the right most digit
            }while(number != 0);

            return sum + leftDigit;

        }
    }
}
