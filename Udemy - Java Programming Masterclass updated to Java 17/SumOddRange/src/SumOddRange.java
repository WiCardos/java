public class SumOddRange {
    public static boolean isOdd(int number) {
        if(number <= 0){
            return false;
        }else return number % 2 != 0;
    }
    public static int sumOdd (int start, int end){
        int count =0;
        if(end < start || start <= 0){ // || end <= 0){ //there is no need for this, for it to be true one of the previous is also true.
            return -1;
        }else{
            for(int i = start; i<= end; i++){
                if(isOdd(i)){
                    count += i;
                }
            }
            return count;
        }
    }
}