public class Main {
    public static void main(String[] args) {
        System.out.println(LastDigitChecker.isValid(10));
        //should return true

        System.out.println(LastDigitChecker.isValid(468));
        //should return true

        System.out.println(LastDigitChecker.isValid(1051));
        //should return false

        System.out.println(LastDigitChecker.hasSameLastDigit(41,22,71));
        //should return true

        System.out.println(LastDigitChecker.hasSameLastDigit(23,32,42));
        //should return true

        System.out.println(LastDigitChecker.hasSameLastDigit(9,99,999));
        //should return false
    }
}
