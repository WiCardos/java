public class Main {
    public static void main(String[] args) {
        int countPrimes = 0;

        for(int i=0; countPrimes < 3 && i <= 1000; i++){

            if(isPrime(i)){
                System.out.println(i + " is " + i  + "is a prime number");
                countPrimes++;
            }
        }

    }

    public static boolean isPrime(int wholeNumber){

        if(wholeNumber <= 2){
            return (wholeNumber == 2);
        }

        for (int divisor = 2; divisor < wholeNumber; divisor++){
            if(wholeNumber % divisor == 0){

                return false;
            }
        }
        return true;
    }
}
