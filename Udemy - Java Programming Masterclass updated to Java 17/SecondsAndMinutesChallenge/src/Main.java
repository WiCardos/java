public class Main {
    public static void main(String[] args) {
        System.out.println(getDurationString(10));
        System.out.println(getDurationString(10,10));
        System.out.println(getDurationString(70));
        System.out.println(getDurationString(70,70));
        System.out.println(getDurationString(3700));
        System.out.println(getDurationString(-10));
        System.out.println(getDurationString(-10, 10));
        System.out.println(getDurationString(10, 100));
        System.out.println(getDurationString(65, 145));
    }
    public static String getDurationString(int seconds){
        if (seconds < 0){
            return "Invalid dara for seconds(" + seconds + "), must be a positive integer value.";
        }else {
            return getDurationString(seconds / 60, seconds % 60);
        }
    }
    public static String getDurationString(int minutes, int seconds){
        if (minutes < 0 ){
            return "Invalid dara for minutes(" + minutes + "), must be a positive integer value.";
        }else if(seconds < 0 || seconds > 59){
            return "Invalid dara for seconds(" + seconds + "), must be a positive integer value between 0 and 59.";
        }else {
            minutes += seconds / 60;
            return minutes / 60 + "h " + minutes % 60 + "m " + seconds % 60 + "s";
        }
    }
}
