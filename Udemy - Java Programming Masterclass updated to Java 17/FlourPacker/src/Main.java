public class Main {
    public static void main(String[] args) {
        System.out.println(FlourPacker.canPack(1, 0, 4));
        //should return false

        System.out.println(FlourPacker.canPack(1, 0, 5));
        //should return true

        System.out.println(FlourPacker.canPack(0, 5,4));
        //should return true

        System.out.println(FlourPacker.canPack(2, 2, 11));
        //should return true

        System.out.println(FlourPacker.canPack(-3, 2, 12));
        //should return false

        System.out.println(FlourPacker.canPack(2, 1, 5));
        //should return true

        System.out.println(FlourPacker.canPack(6, 2, 17));
        //should return true

        System.out.println(FlourPacker.canPack(2, 10, 18));
        //should return true

        System.out.println(FlourPacker.canPack(2, 10, 19));
        //should return true

        System.out.println(FlourPacker.canPack(2, 10, 20));
        //should return true
    }
}
