public class Main {
    public static void main(String[] args) {
        System.out.println(sumDigits(1234));
        System.out.println(sumDigits(-125));
        System.out.println(sumDigits(4));
        System.out.println(sumDigits(32123));
        }
    public static int sumDigits(int number){
        int count = 0;

        if(number <0){
            return -1;
        }


        while(number !=0){
            count += number%10;//last digit, right most digit
            number = number/10;//left most digit
        }
        return count;
    }

}
