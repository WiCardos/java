import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class Games {

    @Test
    void PairOfFivesVsPairOfEights_PairOfEightsWins(){
        //give
        Player player_1 = new Player("5H 5C 6S 7S KD");
        Player player_2 = new Player("2C 3S 8S 8D TD");

        //when
        PokerGame game1 = new PokerGame(player_1, player_2);

        //then
        assertThat(game1.winner()).isEqualTo("P2");
    }

    @Test
    void HighestCardAceVsHighestCardQueen_HighestCardAceWins(){
        //give
        Player player_1 = new Player("5D 8C 9S JS AC");
        Player player_2 = new Player("2C 5C 7D 8S QH");

        //when
        PokerGame game1 = new PokerGame(player_1, player_2);

        //then
        assertThat(game1.winner()).isEqualTo("P1");
    }

    @Test
    void ThreeAcesVsFlushWithDiamonds_FlushWithDiamondsWins(){
        //give
        Player player_1 = new Player("2D 9C AS AH AC");
        Player player_2 = new Player("3D 6D 7D TD QD");

        //when
        PokerGame game1 = new PokerGame(player_1, player_2);

        //then
        assertThat(game1.winner()).isEqualTo("P2");
    }

    @Test
    void PairOfQueensVsPairOfQueens_PairOfQueensWins(){
        //give
        Player player_1 = new Player("4D 6S 9H QH QC");
        Player player_2 = new Player("3D 6D 7H QD QS");

        //when
        PokerGame game1 = new PokerGame(player_1, player_2);

        //then
        assertThat(game1.winner()).isEqualTo("P1");
    }

    @Test
    void FullHouseWithThreeFoursVsFullHouseWithThreeThrees_FullHouseWithThreeFoursWins(){
        //give
        Player player_1 = new Player("2H 2D 4C 4D 4S");
        Player player_2 = new Player("3C 3D 3S 9S 9D");

        //when
        PokerGame game1 = new PokerGame(player_1, player_2);

        //then
        assertThat(game1.winner()).isEqualTo("P1");
    }
}
