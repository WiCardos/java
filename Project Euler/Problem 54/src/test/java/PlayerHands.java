import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PlayerHands {

    @Test
    void Pair_of_Fives(){
        //give
        Player player_1;

        //when
        player_1 = new Player("5H 5C 6S 7S KD");

        //then
        assertThat(player_1.rankedHand()).isEqualTo("One Pair");
        assertThat(player_1.card1Pair).isEqualTo('5');
    }

    @Test
    void Pair_of_Eights(){
        //give
        Player player_2;

        //when
        player_2 = new Player("2C 3S 8S 8D TD");

        //then
        assertThat(player_2.rankedHand()).isEqualTo("One Pair");
        assertThat(player_2.card1Pair).isEqualTo('8');
    }

    @Test
    void Highest_card_Ace(){
        //give
        Player player_1;

        //when
        player_1 = new Player("5D 8C 9S JS AC");

        //then
        assertThat(player_1.rankedHand()).isEqualTo("High Card: A");
    }

    @Test
    void Highest_card_Queen(){
        //give
        Player player_2;

        //when
        player_2 = new Player("2C 5C 7D 8S QH");

        //then
        assertThat(player_2.rankedHand()).isEqualTo("High Card: Q");
    }

    @Test
    void Three_Aces(){
        //give
        Player player_1;

        //when
        player_1 = new Player("2D 9C AS AH AC");

        //then
        assertThat(player_1.rankedHand()).isEqualTo("Three of a Kind");
        assertThat(player_1.cardThreeOfAKind).isEqualTo('A');
    }

    @Test
    void Flush_with_Diamonds(){
        //give
        Player player_2;

        //when
        player_2 = new Player("3D 6D 7D TD QD");

        //then
        assertThat(player_2.rankedHand()).isEqualTo("Flush");
    }

    @Test
    void Pair_of_Queens_9_High(){
        //give
        Player player_1;

        //when
        player_1 = new Player("4D 6S 9H QH QC");

        //then
        assertThat(player_1.rankedHand()).isEqualTo("One Pair");
        assertThat(player_1.card1Pair).isEqualTo('Q');
        assertThat(player_1.getHighCard("Q")).isEqualTo("9");
    }

    @Test
    void Pair_of_Queens_7_High(){
        //give
        Player player_2;

        //when
        player_2 = new Player("3D 6D 7H QD QS");

        //then
        assertThat(player_2.rankedHand()).isEqualTo("One Pair");
        assertThat(player_2.card1Pair).isEqualTo('Q');
        assertThat(player_2.getHighCard("Q")).isEqualTo("7");
    }

    @Test
    void Full_House_With_Three_Fours(){
        //give
        Player player_1;

        //when
        player_1 = new Player("2H 2D 4C 4D 4S");

        //then
        assertThat(player_1.rankedHand()).isEqualTo("Full House");
        assertThat(player_1.cardThreeOfAKind).isEqualTo('4');
    }

    @Test
    void Full_House_with_Three_Threes(){
        //give
        Player player_2;

        //when
        player_2 = new Player("3C 3D 3S 9S 9D");

        //then
        assertThat(player_2.rankedHand()).isEqualTo("Full House");
        assertThat(player_2.cardThreeOfAKind).isEqualTo('3');
    }
}
