import java.util.Map;
import java.util.Hashtable;

public class Player {

    Map<Integer, Character> cardValue = new Hashtable<>();
    private final String hand;

    //I initial though of using only 2 variables (that would work) but it would get confusing
    //and hard to maintain
    public Character cardFourOfAKind;
    public Character cardThreeOfAKind;
    public Character card1Pair;
    public Character card2Pair;

    public Player(String initialHand){
        this.hand = initialHand;

        cardValue.put(1, 'A');
        cardValue.put(2, '2');
        cardValue.put(3, '3');
        cardValue.put(4, '4');
        cardValue.put(5, '5');
        cardValue.put(6, '6');
        cardValue.put(7, '7');
        cardValue.put(8, '8');
        cardValue.put(9, '9');
        cardValue.put(10, 'T');
        cardValue.put(11, 'J');
        cardValue.put(12, 'Q');
        cardValue.put(13, 'K');
        cardValue.put(14, 'A');
    }

    public boolean isFlush() {
        //this was improved by intellij
        return this.hand.substring(1, 2).equals(this.hand.substring(4, 5)) &&
                this.hand.substring(1, 2).equals(this.hand.substring(7, 8)) &&
                this.hand.substring(1, 2).equals(this.hand.substring(10, 11)) &&
                this.hand.substring(1, 2).equals(this.hand.substring(13, 14));
    }
    public String rankedHand(){
        String evaluatedHand = "";
        char l_elem = '0';


        //this loop iterates in the following order:A,K,Q,J,T,9,8,7,6,5,4,3,2,A
        for(Character cha : cardValue.values()){
            switch((int)this.hand.chars().filter(ch -> ch == cha).count()){
                case 4:
                    evaluatedHand = "Four of a Kind";
                    cardFourOfAKind = cha;
                    break;
                case 3:
                    if(evaluatedHand.equals("One Pair")){
                        evaluatedHand = "Full House";
                    }else{
                        evaluatedHand = "Three of a Kind";
                    }
                    cardThreeOfAKind = cha;

                    l_elem = cha;
                    break;
                case 2:
                    if(l_elem == '0'){
                        l_elem = cha;
                        evaluatedHand = "One Pair";
                        card1Pair = cha;
                    }else if(l_elem != cha && evaluatedHand.equals("Three of a Kind")){
                        evaluatedHand = "Full House";
                        card1Pair = cha;
                    }else{
                        evaluatedHand = "Two Pairs";
                        card2Pair = cha;
                    }
                    break;
            }
            //Once it is a four of a king or full house there is no point into continue with the loop
            if(evaluatedHand.equals("Four of a Kind")||evaluatedHand.equals("Full House")){break;}
        }

        for(Integer value : cardValue.keySet()){

            String cha = Character.toString(cardValue.get(value));

            if(this.hand.contains(cha)){
                String value_2 = value+1 <15? Character.toString(cardValue.get(value+1)) : "0";
                String value_3 = value+2 <15? Character.toString(cardValue.get(value+2)) : "0";
                String value_4 = value+3 <15? Character.toString(cardValue.get(value+3)) : "0";
                String value_5 = value+4 <15? Character.toString(cardValue.get(value+4)) : "0";

                if(
                        this.hand.contains(value_2) &&
                        this.hand.contains(value_3) &&
                        this.hand.contains(value_4) &&
                        this.hand.contains(value_5)
                ){
                    evaluatedHand = "Straight";
                    break;  //Since we found a straight there's no point in continue looping.
                }
            }
        }


        if(isFlush() && evaluatedHand.isEmpty()){
            evaluatedHand = "Flush";
        }else if(isFlush()
                && evaluatedHand.equals("Straight")
                && this.hand.contains("T")
                && this.hand.contains("A")){
            evaluatedHand = "Royal Flush";
        }else if(isFlush() && evaluatedHand.equals("Straight")){
            evaluatedHand = "Straight Flush";
        }

        if(evaluatedHand.isEmpty()){
            //this was improved by intellij
            String highCard = getHighCard();
            evaluatedHand = "High Card: " + highCard;
        }

        return evaluatedHand;
    }
    public String getHighCard(String ignoreCards) {
        String highCard = "";

        //this could be an alternative to fixing the hands that have less than 5 high cards bug
//        String highCard = "1";

        if(!ignoreCards.contains("A") && this.hand.contains("A")){
            highCard = "A" ;
        }else if(!ignoreCards.contains("K") && this.hand.contains("K")){
            highCard = "K" ;
        }else if(!ignoreCards.contains("Q") && this.hand.contains("Q")){
            highCard = "Q" ;
        }else if(!ignoreCards.contains("J") && this.hand.contains("J")){
            highCard = "J" ;
        }else if(!ignoreCards.contains("T") && this.hand.contains("T")){
            highCard = "T" ;
        }else if(!ignoreCards.contains("9") && this.hand.contains("9")){
            highCard = "9" ;
        }else if(!ignoreCards.contains("8") && this.hand.contains("8")){
            highCard = "8" ;
        }else if(!ignoreCards.contains("7") && this.hand.contains("7")){
            highCard = "7" ;

        //These are needed for tie breaks, specially on flash hands.
        //Probably not all though (2,3,4 and 5)
        }else if(!ignoreCards.contains("6") && this.hand.contains("6")){
            highCard = "6" ;

        }else if(!ignoreCards.contains("5") && this.hand.contains("5")){
            highCard = "5" ;
        }else if(!ignoreCards.contains("4") && this.hand.contains("4")){
            highCard = "4" ;
        }else if(!ignoreCards.contains("3") && this.hand.contains("3")){
            highCard = "3" ;
        }else if(!ignoreCards.contains("2") && this.hand.contains("2")) {
            highCard = "2" ;

            //this could be an alternative to fixing the hands that have less than 5 high cards bug
//        }else{
//            highCard = "1" ;//+ this.hand.charAt(this.hand.indexOf('2') + 1);
        }
        return highCard;
    }
    //this was improved by intellij
    public String getHighCard() {
        String highCard = "";

        if(this.hand.contains("A")){
            highCard = "A" ;
        }else if(this.hand.contains("K")){
            highCard = "K" ;
        }else if(this.hand.contains("Q")){
            highCard = "Q" ;
        }else if(this.hand.contains("J")){
            highCard = "J" ;
        }else if(this.hand.contains("T")){
            highCard = "T" ;
        }else if(this.hand.contains("9")){
            highCard = "9" ;
        }else if(this.hand.contains("8")){
            highCard = "8" ;
        }else if(this.hand.contains("7")){
            highCard = "7" ;

        //These are needed for tie breaks, specially on flash hands.
        //Probably not all though (2,3,4 and 5)
        }else if(this.hand.contains("6")){
            highCard = "6" ;

        }else if(this.hand.contains("5")){
            highCard = "5" ;
        }else if(this.hand.contains("4")){
            highCard = "4" ;
        }else if(this.hand.contains("3")){
            highCard = "3" ;
        }else if(this.hand.contains("2")) {
            highCard = "2" ;

        }
        return highCard;
    }
}
