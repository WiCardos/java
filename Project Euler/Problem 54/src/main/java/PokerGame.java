import java.util.Hashtable;
import java.util.Map;

public class PokerGame {

    private final Player player_1;
    private final Player player_2;
    Map<String, Integer> highHand = new Hashtable<>();
    public PokerGame(Player player_1, Player player_2) {
        this.player_1 = player_1;
        this.player_2 = player_2;

        //this could be an alternative to fixing the hands that have less than 5 high cards bug
//        highHand.put("High Card: 1",0);

        highHand.put("High Card: 2",1);
        highHand.put("High Card: 3",2);
        highHand.put("High Card: 4",3);
        highHand.put("High Card: 5",4);
        highHand.put("High Card: 6",5);
        highHand.put("High Card: 7",6);
        highHand.put("High Card: 8",7);
        highHand.put("High Card: 9",8);
        highHand.put("High Card: T",9);
        highHand.put("High Card: J",10);
        highHand.put("High Card: Q",11);
        highHand.put("High Card: K",12);
        highHand.put("High Card: A",13);
        highHand.put("One Pair",14);
        highHand.put("Two Pairs",15);
        highHand.put("Three of a Kind",16);
        highHand.put("Straight",17);
        highHand.put("Flush",18);
        highHand.put("Full House",19);
        highHand.put("Four of a Kind",20);
        highHand.put("Straight Flush",21);
        highHand.put("Royal Flush",22);

    }

    public String winner(){
        String result = "Draw";
        String ignoreCards = "";
        String p1Hand  = this.player_1.rankedHand();
        String p2Hand  = this.player_2.rankedHand();
        Integer p1Score  = highHand.get(p1Hand);
        Integer p2Score  = highHand.get(p2Hand);
        int count = 1;


        //in this version the count only need to run 5 times
        //since the first time it runs it already evaluates the high cards
        //and there is a limit of 5 high cards per hand
        while(p1Hand.equals(p2Hand) && count<6) {

            switch (p1Hand) {
                case "One Pair" -> {
                    p1Hand = "High Card: " + this.player_1.card1Pair;
                    p2Hand = "High Card: " + this.player_2.card1Pair;
                    ignoreCards += this.player_1.card1Pair; //just in case they happen to be equal
                }
                case "Two Pair" -> {
                    //Player 1
                    //the highest pair for player 1
                    if (count == 1) {
                        if (
                            highHand.get("High Card: " + this.player_1.card1Pair) >
                            highHand.get("High Card: " + this.player_1.card2Pair)
                        ) {
                            p1Hand = "High Card: " + this.player_1.card1Pair;
                            ignoreCards += this.player_1.card1Pair; //just in case they happen to be equal
                        } else {
                            p1Hand = "High Card: " + this.player_1.card2Pair;
                            ignoreCards += this.player_1.card2Pair; //just in case they happen to be equal
                        }
                    //the lowest pair for player 1
                    } else {
                        if (
                            highHand.get("High Card: " + this.player_1.card1Pair) <
                            highHand.get("High Card: " + this.player_1.card2Pair)
                        ) {
                            p1Hand = "High Card: " + this.player_1.card1Pair;
                            ignoreCards += this.player_1.card1Pair; //just in case they happen to be equal
                        } else {
                            p1Hand = "High Card: " + this.player_1.card2Pair;
                            ignoreCards += this.player_1.card2Pair; //just in case they happen to be equal
                        }
                    }

                    //Player 2
                    //the highest pair for player 2
                    if (count == 1) {
                        if (
                            highHand.get("High Card: " + this.player_2.card1Pair) >
                            highHand.get("High Card: " + this.player_2.card2Pair)
                        ) {
                            p2Hand = "High Card: " + this.player_2.card1Pair;
                        } else {
                            p2Hand = "High Card: " + this.player_2.card2Pair;
                        }
                        //the lowest pair for player 1
                    } else {
                        if (
                            highHand.get("High Card: " + this.player_2.card1Pair) <
                            highHand.get("High Card: " + this.player_2.card2Pair)
                        ) {
                            p2Hand = "High Card: " + this.player_2.card1Pair;
                        } else {
                            p2Hand = "High Card: " + this.player_2.card2Pair;
                        }
                    }
                }
                case "Three of a Kind" -> {
                    p1Hand = "High Card: " + this.player_1.cardThreeOfAKind;
                    p2Hand = "High Card: " + this.player_2.cardThreeOfAKind;
                    ignoreCards += this.player_1.cardThreeOfAKind; //just in case they happen to be equal
                }
                case "Full House" -> {
                    if (count == 1) {
                        p1Hand = "High Card: " + this.player_1.cardThreeOfAKind;
                        p2Hand = "High Card: " + this.player_2.cardThreeOfAKind;
                        ignoreCards += this.player_1.cardThreeOfAKind; //just in case they happen to be equal
                    } else {
                        p1Hand = "High Card: " + this.player_1.card1Pair;
                        p2Hand = "High Card: " + this.player_2.card1Pair;
                        ignoreCards += this.player_1.card1Pair; //just in case they happen to be equal
                    }
                }
                case "Four of a Kind" -> {
                    p1Hand = "High Card: " + this.player_1.cardFourOfAKind;
                    p2Hand = "High Card: " + this.player_2.cardFourOfAKind;
                    ignoreCards += this.player_1.cardFourOfAKind; //just in case they happen to be equal
                }
                default -> {
                    p1Hand = "High Card: " + this.player_1.getHighCard(ignoreCards);
                    p2Hand = "High Card: " + this.player_2.getHighCard(ignoreCards);
                    ignoreCards += this.player_1.getHighCard(ignoreCards);
                }
            }

            p1Score  = highHand.get(p1Hand);
            p2Score  = highHand.get(p2Hand);

            //This is for hands that have less than 5 high cards
            //e.g: 2S 2C 3S 3C 4S vs 2H 2D 3H 3D 4H
            //otherwise this.player_1.getHighCard(ignoreCards) would return ""
            //hence p1Hand would be "High Card: "
            //in turn making p1Score raise a NullPointerException
            if(this.player_1.getHighCard(ignoreCards).isEmpty()){
                break;
            }

            count ++;
        }

        if(p1Score < p2Score){
            result = "P2";
        }else if(p1Score > p2Score) {
            result = "P1";
        }

        return result;
    }
}
