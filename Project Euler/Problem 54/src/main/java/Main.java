import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        File file = new File("C:\\poker.txt");
        try {
            Scanner scanner = new Scanner(file);
            int p1Wins = 0;
            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();

                Player player_1 = new Player(line.substring(0, 14));
                Player player_2 = new Player(line.substring(15, 29));

                PokerGame game1 = new PokerGame(player_1, player_2);

                if(game1.winner().equals("P1")){
                    p1Wins++;
                }
            }
            System.out.println("Player 1 wins " + p1Wins + " wins!");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }
}