import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Palindromes {
    @Test
    public void shouldReturnTrueIfPalindrome() {
        //given
        String string = "1001";
        boolean result;

        //when
        result = Palindrome.isPalindrome(string);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfPalindrome() {
        //given
        String string = Integer.toString(1011);
        boolean result;

        //when
        result = Palindrome.isPalindrome(string);

        //then
        assertThat(result).isFalse();
    }
}
