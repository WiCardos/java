public interface Products {
    static int productsOf3(){

        int largestPalindrome = 0;
        int testingPalindrome;
        String string;

        for(int i=100; i<1000; i++){
            for(int j=i; j<1000; j++){

                testingPalindrome = i * j;
                string = Integer.toString(testingPalindrome);

                if (Palindrome.isPalindrome(string)
                        && testingPalindrome > largestPalindrome) {
                    largestPalindrome = testingPalindrome;
                }
            }
        }
        return largestPalindrome;
    }
}
