public interface Palindrome {
    static boolean isPalindrome(String string){
        boolean palindrome = true;
        int stringLength = string.length();
        for(int i = 0; i < stringLength/2; i++){
            if (string.charAt(i) != string.charAt(stringLength - 1 - i)) {
                palindrome = false;
                break;
            }
        }
        return palindrome;
    };
}
