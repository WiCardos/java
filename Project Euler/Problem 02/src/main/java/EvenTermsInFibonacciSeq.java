public class EvenTermsInFibonacciSeq {
    public static int sumOfEvensInFibonacciSeq(int termMaxValue) {
        int sum;
        int a = 1;
        int b = 2;
        int c = 0;

        sum = termMaxValue < 2 ? 0 : 2;

        while(a + b <= termMaxValue){
            c = a + b;
            if (c % 2 == 0) {
                sum += c;
            }
            a = b;
            b = c;
        }
        return sum;
    }
}
