import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumOfTerms {
    @Test
    void maxTermIs1_shouldReturn0() {
        //given
        int maxValue = 1;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(0, sum);
    }

    @Test
    void maxTermIs2_shouldReturn2() {
        //given
        int maxValue = 2;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(2, sum);
    }

    @Test
    void maxTermIs5_shouldReturn2() {
        //given
        int maxValue = 5;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(2, sum);
    }

    @Test
    void maxTermIs8_shouldReturn10() {
        //given
        int maxValue = 8;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(10, sum);
    }

    @Test
    void maxTermIs10_shouldReturn10() {
        //given
        int maxValue = 10;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(10, sum);
    }

    @Test
    void maxTermIs20_shouldReturn10() {
        //given
        int maxValue = 20;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(10, sum);
    }

    @Test
    void maxTermIs100_shouldReturn10() {
        //given
        int maxValue = 100;
        int sum;

        //when
        sum = EvenTermsInFibonacciSeq.sumOfEvensInFibonacciSeq(maxValue);

        //then
        assertEquals(44, sum);
    }

}
