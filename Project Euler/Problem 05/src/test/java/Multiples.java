import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class Multiples {
    @Test
    public void shouldReturn2520() {
        //given
        int maxFactor = 10;
        int result;

        //when
        result = SmallestMultiple.findSmallestMultiple(maxFactor);

        //then
        Assertions.assertEquals(2520, result);
    }
}
