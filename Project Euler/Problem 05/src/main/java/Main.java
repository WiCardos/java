import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(Main.class.getName());
        logger.info("Smallest positive number evenly divisible by 1 to 20: " + SmallestMultiple.findSmallestMultiple(20));
    }
}
