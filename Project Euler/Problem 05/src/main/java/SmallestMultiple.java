public interface SmallestMultiple {

    //could be improved by using primes (2, 3, 5, 7, 11, 13, 17, 19)
    //instead of validating all elements from 1 to 20
    //Another approach, is to find the exponent alpha of every prime p
    //between 1 and 20
    //that is minor or equal to 20 p^alpha <= 20
    //alpha = log p (20) = ln(20)/ln(p)
    static int findSmallestMultiple(int maxFactor){
        int number = maxFactor + 1;
        boolean found = false;
        boolean multiple;

        while (!found){
            multiple = true;

            for(int i = 1; i <= maxFactor; i++){
                if(!(number%i == 0)){
                    multiple = false;
                    break;
                }
            }
            if(multiple){
                found = true;
                break;
            }
            number++;
        }
        return number;
    }
}
