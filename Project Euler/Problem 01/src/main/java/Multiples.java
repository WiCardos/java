import java.util.ArrayList;

public class Multiples {

    public static int sumOfMultiplesOf3And5(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) {
            if(i%3==0 || i%5==0) {
                sum += i;
            }
        }
        return sum;
    }
}
