import org.junit.jupiter.api.Test;

import java.util.ArrayList;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class Sums {
    @Test
    void sumUpTo10_shouldReturn23(){
        //give
        int number = 10;
        int sum;

        //when
        sum = Multiples.sumOfMultiplesOf3And5(number);

        //then
        assertEquals(23, sum);
    }
}
