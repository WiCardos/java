import java.io.*;
import java.util.ArrayList;

//Not necessary for problem 03
public interface PrimeList {

    static void readFileToArray(ArrayList<Integer> array, String file){
        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while (reader.ready()) {
                array.add(Integer.parseInt(reader.readLine()));
            }
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    static void writeFileToArray(ArrayList<Integer> array, String file){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for (Integer elem : array) {
                writer.write(elem + "\n");
            }
            writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
