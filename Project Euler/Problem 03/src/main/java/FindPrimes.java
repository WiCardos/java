import java.util.ArrayList;

//Not necessary for problem 03
public interface FindPrimes extends PrimeList {
    ArrayList<Integer>  array = new ArrayList();
    String file = "PrimeList.txt";


    static void findPrimes(long number){

        PrimeList.readFileToArray(array, file);
        int lastElement = array.size() - 1;

        Boolean isNotPrime;

        if(array.get(lastElement)<number) {

            for (int i = (array.get(lastElement)+1); i < number; i++) {

                isNotPrime = false;

                for(Integer prime : array) {
                    if(i%prime==0){
                        isNotPrime = true;
                        break;
                    }else if(Math.pow(prime,2)>i){
                        break;
                    }
                }

                if (!isNotPrime){
                    array.add(i);
                }

            }
        }

        PrimeList.writeFileToArray(array, file);

    }
}
