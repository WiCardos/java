import java.util.ArrayList;

public interface FindPrimeFactors {

    static int findLargestPrimeFactor(long number){
        ArrayList<Long> array = new ArrayList<>();
        long totalMultiplication = 1;
        long firstFactor;
        long secondFactor = number;

        while(totalMultiplication < number){
            totalMultiplication = 1;

            firstFactor = findFactors(secondFactor).first();
            secondFactor = findFactors(secondFactor).second();
            array.add(firstFactor);

            for(Long elem: array){
                totalMultiplication = totalMultiplication * elem;
            }
            System.out.println(array);
        }

        return Math.toIntExact(array.get(array.size() - 1));
    }

    static Factors findFactors(long number){
        long firstFactor = 1;
        long secondFactor = 1;

        for(int i=2; i<=number; i++){
            if(number % i == 0){
                firstFactor = i;
                secondFactor = number / i;
                break;
            }
        }
        return new Factors(firstFactor, secondFactor);
    }
}
