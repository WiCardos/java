public class Main implements PrimeList, FindPrimes, FindPrimeFactors {
    public static void main(String[] args) {
        long number = 600851475143L;
        System.out.println("larger factor: " +FindPrimeFactors.findLargestPrimeFactor(number));
    }
}
