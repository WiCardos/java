import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class findPrimes {
    @Test
    void shouldReturn5(){
        //give
        int number = 10;
        int result;

        //when
        result = FindPrimeFactors.findLargestPrimeFactor(number);

        //then
        assertEquals(5, result);
    }

    @Test
    void shouldAlsoReturn5(){
        //give
        int number = 20;
        int result;

        //when
        result = FindPrimeFactors.findLargestPrimeFactor(number);

        //then
        assertEquals(5, result);
    }
    @Test
    void shouldReturn29(){
        //give
        int number = 13195;
        int result;

        //when
        result = FindPrimeFactors.findLargestPrimeFactor(number);

        //then
        assertEquals(29, result);
    }
}
